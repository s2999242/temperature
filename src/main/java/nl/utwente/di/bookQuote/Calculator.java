package nl.utwente.di.bookQuote;

public class Calculator {
    public double calculate(double temperature) {
        return temperature * ((double) 9 / 5) + 32;
    }
}
